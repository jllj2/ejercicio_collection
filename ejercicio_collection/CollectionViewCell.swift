//
//  CollectionViewCell.swift
//  ejercicio_collection
//
//  Created by mastermoviles on 9/11/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
